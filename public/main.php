<?php

// ini_set('display_errors', 1);
// error_reporting(E_ALL);

$contact = isset($_GET['contact']) ? htmlspecialchars($_GET['contact']) : '';

if (!empty($contact)) {
  $emailTo = 'dhon-velogon@yandex.ru';

  $headers  = 'MIME-Version: 1.0'."\r\n";
  $headers .= 'Content-type: text/html; charset=UTF-8'."\r\n";
  $headers .= 'To: Mary <'.$emailTo.'>'."\r\n";
  $headers .= 'From: Elephant design'."\r\n";
  $headers .= 'Cc: '.$emailTo."\r\n";
  $headers .= 'Bcc: '.$emailTo."\r\n";

  $subject = 'Новая заявка';

  $message = "
  <br>Контакт: ".$contact;
  $message .= "
  <br>
  <br>IP-адрес посетителя: ".@$_SERVER['REMOTE_ADDR']."
  <br>Время заявки: ".date('H:i d.m.Y')."
  ";

  $mail = mail($emailTo, $subject, $message, $headers);

  $json = 'success';
} else {
  $json = 'error';
}

echo json_encode($json, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
exit;
