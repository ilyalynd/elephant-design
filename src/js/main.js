(function () {
  'use strict'

  const html = document.querySelector('html');


  // Плавно прокручивает к якорю
  const anchors = document.querySelectorAll('[data-action=scroll-to]');

  function scrollTo(event) {
    let redirection = this.getAttribute('href').split('#')[0],
        sectionId = this.getAttribute('href').split('#')[1];

    if (redirection != '/') event.preventDefault();

    closeMenu(event);

    document.getElementById(sectionId).scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    });
  }

  anchors.forEach(anchor => anchor.addEventListener('click', scrollTo));


  // Открывает меню
  const linkOpenMenu = document.querySelectorAll('[data-action=open-menu]');

  function openMenu(event) {
    event.preventDefault();

    let menu = document.querySelector('.menu');

    menu.classList.add('menu--open');
    html.style.overflowY = 'hidden';

    event.stopPropagation();
  }

  linkOpenMenu.forEach(link => link.addEventListener('click', openMenu));


  // Закрывает меню
  const linkCloseMenu = document.querySelectorAll('[data-action=close-menu]');

  function closeMenu(event) {
    let url = window.location.pathname;

    if (url == '/') event.preventDefault();

    let menu = document.querySelector('.menu');

    menu.classList.remove('menu--open');
    html.style.overflowY = 'visible';

    event.stopPropagation();
  }

  linkCloseMenu.forEach(link => link.addEventListener('click', closeMenu));


  // Обрабатывает отправку форм
  const buttons = document.querySelectorAll('[data-action=send-form]');

  function submitForm(event) {
    event.preventDefault();

    let form = this.closest('form'),
        formName = form.getAttribute('name');

    // Добавляет disabled к кнопке, при попытке отправить пустую форму
    checkInput(form);

    if (this.classList.contains('disabled')) {
      // Подсвечивает незаполненные поля
      form.querySelectorAll('.empty').forEach(selector => selector.classList.add('error'));

      setTimeout(() => {
        form.querySelectorAll('.empty').forEach(selector => selector.classList.remove('error'));
      }, 1500);
    }

    else {
      // Отправляет форму
      const request = new XMLHttpRequest();

      let contact = form.querySelector('input[name=contact]'),
          url = '/main.php?contact=' + contact; // Адрес файла принимающего данные формы

      request.open('GET', url);
      request.setRequestHeader('Content-Type', 'application/x-www-form-url');
      request.addEventListener('readystatechange', () => {
        if (request.readyState === 4 && request.status === 200) {
          console.log(request.responseText);
        }
      });

      // Очищает заполненные поля формы
      form.querySelectorAll('input').forEach(selector => {
        selector.value = '';
      });
    }
  }

  // Проверяет заполнение обязательных полей формы и блокирует кнопку
  function checkInput(form) {
    let button = form.querySelector('[data-action=send-form]');

    form.querySelectorAll('input').forEach(selector => {
      if (selector.hasAttribute('required')) {
        if (selector.value.length < 1) {
          selector.classList.add('empty');
        } else {
          selector.classList.remove('empty');
        }
      }
    });

    let countEmpty = form.querySelectorAll('.empty').length;

    if (countEmpty > 0) {
      if (!button.classList.contains('disabled')) {
        button.classList.add('disabled');
      }
    } else {
      button.classList.remove('disabled');
    }
  }

  buttons.forEach(button => button.addEventListener('click', submitForm));
})();
